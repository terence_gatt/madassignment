package com.example.assignmentapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class MainActivity extends AppCompatActivity{
    private static final String password = "stop";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_pinview);

        Pinview pin = (Pinview)findViewById(R.id.pinview);
        pin.setPinViewEventListener(new Pinview.PinViewEventListener()
        {
            @Override
            public void onDataEntered(Pinview pinview, boolean a) {
                String s = pinview.getValue();
                if (s.equalsIgnoreCase(password))
                {
                    Toast.makeText(MainActivity.this, "Correct password", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(MainActivity.this, Home.class);
                    startActivity(i);
                }
                else
                {
                    Toast.makeText(MainActivity.this, "Incorrect password", Toast.LENGTH_SHORT).show();
                    pinview.setValue("");
                }
        }
        });
    }
}