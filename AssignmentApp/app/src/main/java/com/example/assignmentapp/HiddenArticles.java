package com.example.assignmentapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

public class HiddenArticles extends AppCompatActivity implements MyRecycleViewAdapter.ItemClickListener,
        NavigationView.OnNavigationItemSelectedListener{
    private static final String TAG = "Hidden";
    MyRecycleViewAdapter adapter;
    ArrayList<String> TextArticles = new ArrayList<String>();
    private SharedPreferences mPrefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer3);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        getArticles();

    }

    protected void GetHidden(DatabaseReference myRef){
        Query lastQuery = myRef.orderByKey().limitToLast(500);
        lastQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> test = new ArrayList<String>();
                try {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        String p = postSnapshot.getKey();
                        String a = postSnapshot.child("title").getValue().toString();
                        System.out.println(a);
                        test.add(a);
                        Log.i(TAG, "Read successful. ");
                    }
                }catch(IllegalArgumentException e)
                {

                }
                TextArticles = test;
                RecyclerView recyclerView = findViewById(R.id.TextArticles);
                recyclerView.setLayoutManager(new LinearLayoutManager(HiddenArticles.this));
                adapter = new MyRecycleViewAdapter(HiddenArticles.this, TextArticles);
                adapter.setClickListener(HiddenArticles.this);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.i(TAG, "Error in reading data");

            }
        });
    }
    public void getArticles(){

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Hidden: ");
        DatabaseReference dr = myRef.child("post");
        GetHidden(dr);
        RecyclerView recyclerView = findViewById(R.id.TextArticles);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyRecycleViewAdapter(this, TextArticles);
        adapter.setClickListener(HiddenArticles.this);
        recyclerView.setAdapter(adapter);
    }
    public void onItemClick(View view, final int position) {
    }
    public boolean onCreateOptionsMenu(Menu m){

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, m);
        return true;

    }
    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){

            case R.id.mwiki: {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://en.wikipedia.org/"));
                startActivity(i);
                break;
            }
        }

        return true;

    }

    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Intent i = new Intent(HiddenArticles.this, Home.class);
            startActivity(i);
        } else if (id == R.id.nav_gallery) {
            Intent i = new Intent(HiddenArticles.this, FavouritesActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_view) {
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        String date = savedInstanceState.getString("dateusedhidden");
        if (date != null)
        {
            final View parentLayout = findViewById(android.R.id.content);
            Snackbar.make(parentLayout, "Date since last use: " +date, Snackbar.LENGTH_LONG).show();
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outstate){
        super.onSaveInstanceState(outstate);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String state2 = mdformat.format(calendar.getTime());
        outstate.putString("dateusedhidden", state2);
    }
}

