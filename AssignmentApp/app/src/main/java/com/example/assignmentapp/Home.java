package com.example.assignmentapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

public class Home extends AppCompatActivity implements MyRecycleViewAdapter.ItemClickListener,
                                                        NavigationView.OnNavigationItemSelectedListener{
    private static final String TAG = "Home";
    MyRecycleViewAdapter adapter;
    ArrayList<ArrayList<String>> storage = new ArrayList<ArrayList<String>>();
    ArrayList<String> TextArticles = new ArrayList<String>();
    int pos;
    int id;
    String loc;
    Context context = Home.this;
    private SharedPreferences mPrefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Button btnadd = findViewById(R.id.button2);
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Home.this, NewArticle.class);
                startActivity(i);
            }
        });
    }

    public void getArticles(View v){

        ArrayList<String>temp = new ArrayList<String>();
        WikipediaTask task = new WikipediaTask();
        String t = "search";
        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        DatabaseReference myRef1 = database1.getReference("Article: ");
        GetNewArticles(myRef1);
        try {
            JSONArray res = task.execute(t).get();
            for(int i =0; i < res.length(); i++)
            {
                JSONObject rec = res.getJSONObject(i);
                Log.i(String.valueOf(rec.getInt("count")), "Json item" +i);
                id = rec.getInt("count");
                loc = rec.getString("title");
                storage.add(new ArrayList<String>() {
                    {
                        add("ID: ");
                        add(""+id);
                        add("Title");
                        add(loc);
                    }
                });
                temp.add("ID: "+ id + "\nTitle: "+loc);
            }


        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        TextArticles = temp;
        RecyclerView recyclerView = findViewById(R.id.TextArticles);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyRecycleViewAdapter(this, temp);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        FirebaseDatabase database2 = FirebaseDatabase.getInstance();
        DatabaseReference myRef2 = database2.getReference("Hidden: ");
        GetHidden(myRef2);
    }

    public void onItemClick(View view, int position) {
        pos = position;
        PopupMenu popupMenu = new PopupMenu(this, view);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final View parentLayout = findViewById(android.R.id.content);
                switch(item.getItemId()){
                    case R.id.pitm1:
                        ArrayList<String> a = storage.get(pos);
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference myRef = database.getReference("Favourite: ");
                        Post post = new Post(a.get(1).toString(), a.get(3));
                        myRef.child("post").push().setValue(post);
                        Snackbar snack = Snackbar.make(parentLayout, "Added to favourites.", Snackbar.LENGTH_LONG)
                                .setAction(" View Favourites", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent i = new Intent(Home.this, FavouritesActivity.class);
                                        startActivity(i);
                                    }
                                });;
                        snack.show();

                        break;
                    case R.id.pitm2:
                        ArrayList<String> a2 = storage.get(pos);
                        FirebaseDatabase database2 = FirebaseDatabase.getInstance();
                        DatabaseReference myRef2 = database2.getReference("Hidden: ");
                        Post post2 = new Post(a2.get(1).toString(), a2.get(3));
                        myRef2.child("post").push().setValue(post2);
                        Snackbar.make(parentLayout, "Article hidden", Snackbar.LENGTH_LONG).show();
                        GetHidden(myRef2);
                        break;
                    default: return false;

                }
                return true;
            }
        });

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup_menu, popupMenu.getMenu());
        popupMenu.show();
    }

    public boolean onCreateOptionsMenu(Menu m){

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, m);
        return true;

    }
    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){

            case R.id.mwiki: {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://en.wikipedia.org/"));
                startActivity(i);
                break;
            }
        }

        return true;

    }

    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
        } else if (id == R.id.nav_gallery) {
            Intent i = new Intent(Home.this, FavouritesActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_view) {
            Intent i = new Intent(Home.this, HiddenArticles.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public static class Post {

        public String ID;
        public String title;

        public Post(String id, String Title) {
            ID = id;
            title = Title;
        }
        public Post(){};

    }

    protected void GetHidden(DatabaseReference myRef){
        myRef = myRef.child("post");
        Query lastQuery = myRef.orderByKey().limitToLast(500);
        lastQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> test2 = TextArticles;
                ArrayList<String> test = new ArrayList<String>();
                try {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        String p = postSnapshot.getKey();
                        String a = postSnapshot.child("title").getValue().toString();
                        System.out.println(a);
                        test.add(a);
                        Log.i(TAG, "Read successful. ");
                    }
                    for (int i =0; i < storage.size(); i++)
                    {
                        for (int j = 0; j < test.size(); j++) {
                            if (storage.get(i).get(3) == test.get(j))
                            {
                                test2.remove(i);
                                storage.remove(i);
                                TextArticles.remove(i);
                            }
                        }
                    }
                }catch(IllegalArgumentException e)
                {

                }
                for (int i =0; i < storage.size(); i++)
                {
                    for (int j = 0; j < test.size(); j++) {
                        if (storage.get(i).get(3) == test.get(j))
                        {
                            test2.remove(i);
                            storage.remove(i);
                            TextArticles.remove(i);
                        }
                    }
                }
                TextArticles = test2;
                RecyclerView recyclerView = findViewById(R.id.TextArticles);
                recyclerView.setLayoutManager(new LinearLayoutManager(Home.this));
                adapter = new MyRecycleViewAdapter(Home.this, TextArticles);
                adapter.setClickListener(Home.this);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.i(TAG, "Error in reading data");

            }
        });
    }
    ArrayList<String> x = new ArrayList<String>();
    ArrayList<String> y = new ArrayList<String>();
    int h;
    protected void GetNewArticles(DatabaseReference myRef){

        Query lastQuery = myRef.orderByKey().limitToLast(500);
        lastQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> test2 = TextArticles;
                ArrayList<String> test = new ArrayList<String>();
                try {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        String p = postSnapshot.getKey();
                        String a = postSnapshot.getValue().toString();
                        x.add(p);
                        y.add(a);
                        System.out.println(a);
                        test.add(p + "\n" + a);
                        Log.i(TAG, "Read successful. ");
                    }
                    for (h = 0; h < test.size(); h++) {
                            test2.add(test.get(h));
                            storage.add(new ArrayList<String>() {
                                {
                                    add("ID: ");
                                    add(x.get(h));
                                    add("Title");
                                    add(y.get(h));
                                }
                            });
                            TextArticles.add(test.get(h));
                    }
                }catch(IllegalArgumentException e)
                {

                }
                TextArticles = test2;
                RecyclerView recyclerView = findViewById(R.id.TextArticles);
                recyclerView.setLayoutManager(new LinearLayoutManager(Home.this));
                adapter = new MyRecycleViewAdapter(Home.this, TextArticles);
                adapter.setClickListener(Home.this);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.i(TAG, "Error in reading data");

            }
        });
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        String date = savedInstanceState.getString("dateusedhome");
        if (date != null)
        {
            final View parentLayout = findViewById(android.R.id.content);
            Snackbar.make(parentLayout, "Date since last use: " +date, Snackbar.LENGTH_LONG).show();
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outstate){
        super.onSaveInstanceState(outstate);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String state2 = mdformat.format(calendar.getTime());
        outstate.putString("dateusedhome", state2);
    }


}
