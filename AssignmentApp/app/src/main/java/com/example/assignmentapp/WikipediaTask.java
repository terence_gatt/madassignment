package com.example.assignmentapp;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WikipediaTask extends AsyncTask<String, Void, JSONArray>{

    protected JSONArray doInBackground(String... Articles) {

        String temp = "";
        JSONArray pages = null;

        GetArticleHttpClient client = new GetArticleHttpClient();

        String resultJSON = client.GetArticles();

        try {
            JSONObject jObj = new JSONObject(resultJSON);

            JSONObject mainObj = jObj.getJSONObject("query");
            pages = mainObj.getJSONArray("mostviewed");
            temp = pages.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return pages;
    }
}
