package com.example.assignmentapp;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetArticleHttpClient {

    private static String BASE_URL = "https://en.wikipedia.org/w/api.php?action=query&format=json&list=mostviewed&pvimlimit=500&meta=&titles=Project%3AarticleA%7Carticle_B&pvimmetric=pageviews";


    public String GetArticles() {
        HttpURLConnection con = null ;
        InputStream is = null;

        try {
            String url = BASE_URL;

            //add app ID


            con = (HttpURLConnection) ( new URL(url)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();

            // Let's read the response
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while (  (line = br.readLine()) != null )
                buffer.append(line + "\r\n");

            is.close();
            con.disconnect();

            return buffer.toString();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }

        return null;

    }
}
