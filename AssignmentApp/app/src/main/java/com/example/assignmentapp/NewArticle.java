package com.example.assignmentapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class NewArticle extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    EditText tvMsg;
    String e;
    private SharedPreferences mPrefs;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer4);
        tvMsg = findViewById(R.id.txtnew);
        e = tvMsg.getText().toString();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Button btnadd = findViewById(R.id.btnadd);
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txtnew = tvMsg.getText().toString();
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("Article: ");
                myRef.push().setValue(txtnew);
                Toast.makeText(NewArticle.this, "Article added", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(NewArticle.this, Home.class);
                startActivity(i);
            }
        });
    }
    public boolean onCreateOptionsMenu(Menu m){

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, m);
        return true;

    }
    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){

            case R.id.mwiki: {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://en.wikipedia.org/"));
                startActivity(i);
                break;
            }
        }

        return true;

    }

    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Intent i = new Intent(NewArticle.this, Home.class);
            startActivity(i);
        } else if (id == R.id.nav_gallery) {
            Intent i = new Intent(NewArticle.this, FavouritesActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_view) {
            Intent i = new Intent(NewArticle.this, HiddenArticles.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        String state = savedInstanceState.getString("saved_state");
        String date = savedInstanceState.getString("dateused");
        if (state == null){
            Toast.makeText(NewArticle.this, "Not saved!", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(NewArticle.this, "Restored text: \n" + state, Toast.LENGTH_LONG).show();
            tvMsg.setText(state);
        }
        if (date != null)
        {
            final View parentLayout = findViewById(android.R.id.content);
            Snackbar.make(parentLayout, "Date since last use: " +date, Snackbar.LENGTH_LONG).show();
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outstate){
        super.onSaveInstanceState(outstate);
        String statesave = tvMsg.getText().toString();
        outstate.putString("saved_state", statesave);
        Toast.makeText(NewArticle.this, "Saved state: "+ statesave, Toast.LENGTH_LONG).show();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String state2 = mdformat.format(calendar.getTime());
        outstate.putString("dateused", state2);
    }
}
